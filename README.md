# Splork

A random sentence generator written in Flutter and Dart for Android and iOS.

## Getting Started

You need the Android Studio, Flutter plug in, and a whole mess of patience as you download the packages and generate the apps.