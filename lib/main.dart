/*
main.dart - the main guts of the application

Copyright (C) 2021 William R. Moore <caranmegil@gmail.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:splork/splork.dart';
import 'package:share/share.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Splork',
      theme: ThemeData(
        primaryColor: Color(0xFF40826D),
        accentColor: Color(0xFF40826D),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Splork'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _message = "";

  Future<Splork> fetchSplork() async {
    final response = await http.get('https://splork.herokuapp.com/message');

    if (response.statusCode == 200) {
      return Splork.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load message!');
    }
  }

  void splork() {
    fetchSplork().then((value) => {
      setState(() {
        _message = value.message;
      })
    }).catchError((onError) => {
      setState(() {
        _message = onError.toString();
      })
    });
  }

  onShare(BuildContext context) async {
    await Share.share(_message,
        subject: "Splork"
    );
  }

  @override
  void initState() {
    super.initState();
    _message = "Click the button to generate a message";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 25.0),
            child: GestureDetector(
              onTap: () {
                onShare(context);
              },
              child: Icon(
                Icons.share
              )
            )
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '$_message',
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: splork,
        tooltip: 'Generate',
        child: Icon(Icons.send),
      ),
    );
  }
}
